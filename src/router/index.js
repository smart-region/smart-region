import Vue from 'vue'
import store from '../store'
import VueRouter from 'vue-router'
import Addresses from "../views/Pages/Addresses/Addresses";
import RadaPage from "../views/Pages/Rada/RadaPage";
import MiskRada from "../views/Pages/MiskRada/MiskRada.vue";
import Medicine from "../views/Pages/Medicine/Medicine.vue";
import Transport from "../views/Pages/Transport/Transport.vue";
import Education from "../views/Pages/Education/Education.vue";
import Fine from "../views/Pages/Fine/Fine.vue";
import MyRequestsPage from "../views/Pages/Rada/MyRequestsPage";
import CreateRequestPage from "../views/Pages/Rada/CreateRequestPage";
import RequestItem from "../views/Pages/Rada/RequestItem";
import CommunalListPage from "../views/Pages/Communal/CommunalListPage";
import AllServices from "../views/Pages/Communal/AllServices";
import CompanyPage from "../views/Pages/Communal/CompanyPage";
import testVue from "../views/testVue";
import NotificationsPage from "../views/Pages/NotificationsPage";
import Intro from "../views/Pages/Intro";
import OneNotification from "../views/Pages/OneNotification";
import GiftPage from "../views/Pages/GiftPage";
import RequestSendSuccess from "../views/Pages/Rada/RequestSendSuccess";
import InputMeterage from "../views/Pages/Communal/InputMeterage";
import PreviewPage from "../views/PreviewPage"
import AddAddress from "../views/Pages/Addresses/AddAddress"
import AddAddressManual from "../views/Pages/Addresses/AddAddressManual"
import AddressMap from "../views/Pages/Addresses/AddressMap"
import AddLic from "../views/Pages/Addresses/AddLic"
import BeforeAddAddress from "../views/Pages/Addresses/BeforeAddAddress"
import AddCompanyToAddress from "../views/Pages/Communal/AddCompanyToAddress"
import GetHistoryMeterage from "../views/Pages/Communal/GetHistoryMeterage";
import GetHistoryPay from "../views/Pages/Communal/GetHistoryPay";
import AddedAddress from "../views/Pages/Addresses/AddedAddress"
import AddedBad from "../views/Pages/Addresses/AddedBad"
import Payment from "../views/Pages/Communal/Payment";
import UsersFeedback from "../views/Pages/Communal/UsersFeedback";
import ServiceOrder from "../views/Pages/Communal/ServiceOrder";
import FeedbackForm from "../views/Pages/Communal/FeedbackForm";
import ShowFeedback from "../views/Pages/Communal/ShowFeedback";
import AddLicToAddress from "../views/Pages/Communal/AddLicToAddress"
import AddCounter from "../views/Pages/Communal/AddCounter"
import AllPayment from "../views/Pages/AllPayment/AllPayment.vue"
import CardSelection from "../views/Pages/AllPayment/CardSelection.vue"
import VisualPay from "../views/Pages/AllPayment/VisualPay.vue"
import Receipt from "../views/Pages/AllPayment/Receipt"
import Seting from "../views/components/Seting.vue"
import Preloader from "../views/components/Preloader.vue"
import SetingAddress from "../views/components/SetingAddress.vue"
import Frame from "../views/Pages/AllPayment/Frame.vue"



const routes = [
    {
      path: "/main/:token/:load/:num",
      name: 'Push',
      component: Addresses
    },
    {
      path: "/main/:token/:lang/:ln",
      name: 'Lang',
      component: Addresses
    },
    {
      path: "/main/:token",
      name: 'Addresses',
      component: Addresses
    },
    {
      path: '/radaObl',
      name: 'RadaPage',
      component: RadaPage
    },
    {
      path: '/radaMisk',
      name: 'MiskRada',
      component: MiskRada
    },
    {
      path: '/medicine',
      name: 'Medicine',
      component: Medicine
    },
    {
      path: '/transport',
      name: 'Transport',
      component: Transport
    },
    {
      path: '/education',
      name: 'Education',
      component: Education
    },
    {
      path: '/fine',
      name: 'Fine',
      component: Fine
    },
    {
      path: '/myRequests/:type/:token',
      name: 'MyRequestsPage',
      component: MyRequestsPage
    },
    {
      path: '/myRequest/:type/:token',
      name: 'CreateRequestPage',
      component: CreateRequestPage
    },
    {
      path: '/requestItem/:type',
      name: 'RequestItem',
      component: RequestItem
    },
    {
      path: '/communalList',
      name: 'CommunalListPage',
      component: CommunalListPage,
      props: true
    },
    {
      path: '/services/all',
      name: 'AllServices',
      component: AllServices
    },
    {
      path: '/company',
      name: 'CompanyPage',
      component: CompanyPage,
      props: true
    },
    {
      path: '/notifications',
      name: 'NotificationsPage',
      component: NotificationsPage
    },
    {
      path: '/one_notification/:notification_id',
      name: 'OneNotification',
      component: OneNotification
    },
    {
      path: '/gifts',
      name: 'GiftPage',
      component: GiftPage
    },
    {
      path: '/test',
      name: 'testVue',
      component: testVue
    },
    {
      path: '/request/success/page',
      name: 'RequestSendSuccess',
      component: RequestSendSuccess
    },
    {
      path: '/meterage_input',
      name: 'InputMeterage',
      component: InputMeterage,
      props: true
    },
    {
      path: '/previewPage',
      name: 'PreviewPage',
      component: PreviewPage,
    },
    {
      path: '/address/add',
      name: 'AddAddress',
      component: AddAddress,
      props: true
    },
    {
      path: '/address/add/manual',
      name: 'AddAddressManual',
      component: AddAddressManual,
    },
    {
      path: '/address/map',
      name: 'AddressMap',
      component: AddressMap,
      props: true
    },
    {
      path: '/address/add/lic/:id',
      name: 'AddLic',
      component: AddLic,
      props: true
    },
    {
      path: '/address/before/add',
      name: 'BeforeAddAddress',
      component: BeforeAddAddress,
      props: true
    },
    {
      path: '/address/company/add',
      name: 'AddCompanyToAddress',
      component: AddCompanyToAddress,
      props: true
    },
    {
      path: '/communal/meterage/history_meterage',
      name: 'GetHistoryMeterage',
      component: GetHistoryMeterage,
      props: true
    },
    {
      path: '/history_pay',
      name: 'GetHistoryPay',
      component: GetHistoryPay,
      props: true
    },
    {
      path: '/communal/meterage/payment',
      name: 'Payment',
      component: Payment,
      props: true
    },
    {
      path: '/communal/meterage/users_feedback',
      name: 'UsersFeedback',
      component: UsersFeedback,
      props: true
    },
    {
      path: '/communal/meterage/show_feedback',
      name: 'ShowFeedback',
      component: ShowFeedback,
      props: true
    },
    {
      path: '/communal/meterage/service_order',
      name: 'ServiceOrder',
      component: ServiceOrder,
      props: true
    },
    {
      path: '/communal/meterage/new_feedback',
      name: 'FeedbackForm',
      component: FeedbackForm,
      props: true
    },
    {
      path: '/address/company/add/lic/:id',
      name: 'AddLicToAddress',
      component: AddLicToAddress,
      props: true
    },
    {
      path: '/address/added',
      name: 'AddedAddress',
      component: AddedAddress,
      props: true
    },
    {
      path: '/address/added_bad',
      name: 'AddedBad',
      component: AddedBad,
      props: true
    },
    {
      path: '/add_counter',
      name: 'AddCounter',
      component: AddCounter,
      props: true
    },
    {
      path: '/all_payment/:token',
      name: 'AllPayment',
      component: AllPayment,
      props: true
    },
    {
      path: '/card_selection/:token',
      name: 'CardSelection',
      component: CardSelection,
      props: true
    },
    {
      path: '/visual_pay/:token',
      name: 'VisualPay',
      component: VisualPay,
      props: true
    },
    {
      path: '/frame',
      name: 'Frame',
      component: Frame,
      props: true
    },
    {
      path: '/receipt',
      name: 'Receipt',
      component: Receipt,
      props: true
    },
    {
    path: '/seting',
    name: 'Seting',
    component: Seting,
    props: true
    },
    {
    path: '/preloader',
    name: 'Preloader',
    component: Preloader,
    props: true
    },
    {
    path: '/seting_address',
    name: 'SetingAddress',
    component: SetingAddress,
    props: true
    },
    {
    path: '/intro',
    name: 'Intro',
    component: Intro,
    props: true
    },

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});



export default router
