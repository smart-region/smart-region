import Vue from 'vue'
import Vuex from 'vuex'
import hotline from "./modules/hotline";
import storage from "./modules/storage";
import communal from "./modules/communal";
import address from "./modules/address";
import feedback from "./modules/feedback";
import input_meterage from "./modules/input_meterage";
import allPay from "./modules/allPay";
import easypay from "./modules/easypay";
import getAddressInfo from "./modules/getAddressInfo";
import idGovDiia from "./modules/idGovDiia";
import seting from "./modules/seting";
import confirm_company from "./modules/confirm_company";
import historys from "./modules/historys";
import notification from "./modules/notification";
import meterage from "./modules/meterage";
import manual_address from "./modules/manual_address";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
	hotline,
	storage,
	communal,
	address,
	feedback,
	input_meterage,
	allPay,
	easypay,
	getAddressInfo,
	idGovDiia,
	seting,
	confirm_company,
	historys,
	notification,
	meterage,
	manual_address
  }
})
