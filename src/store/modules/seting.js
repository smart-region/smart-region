import axios from "axios";
function initialState () {
        const delAddressStatus = ''
        const delCardStatus = ''
        const companyList = ''
        const attend = ''
        const cardListStatus = ''
    return {
        delAddressStatus,
        delCardStatus,
        companyList,
        attend,
        cardListStatus
    }
}

const getters = {
    DELETE_ADDRESS_STATUS(state) {
        return state.delAddressStatus
    },
    DELETE_CARD_STATUS(state) {
        return state.delCardStatus
    },
    ATTEND(state) {
        return state.attend
    },
    CARD_STATUS_SETING(state) {
        return state.cardListStatus
    },
    
};

const actions = {
    delAddress(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'accounts/delete/' + data, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            ctx.commit('deleteAddressStatus', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    attendUser(ctx) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'attend', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            ctx.commit('setAttendUser', response.data)
            console.log(response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    delCard(ctx, data) {
        return axios('https://kr.smartregion.com.ua/api/region/v1/payment/easy-pay/delete-card', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('deleteCardStatus', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    checkCardListSeting(ctx) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'payment/easy-pay/card-list', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            
        })
        .then((response) => {
            // console.log(response.data)
            ctx.commit('setStatusCardList', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    getCompany(ctx, data) {
        return axios('https://kr.smartregion.com.ua/api/region/v1/accounts/companyLs/' + data, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            }, 
        })
        .then((response) => {
            ctx.commit('setCompanyList', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    }

    
};

const mutations = {
    deleteAddressStatus(state, value) {
        state.delAddressStatus = value
    },

    deleteCardStatus(state, value) {
        state.delCardStatus = value
    },

    setStatusCardList(state, value) {
        state.cardListStatus = value
    },

    setCompanyList(state, list) {
        console.log(list)
        state.companyList = list
    },

    setAttendUser(state, value) {
        state.attend = value
    }
    
    
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
