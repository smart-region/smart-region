//import { BASE_URL } from "./modulsConsts"

const BASE_URL = 'https://kr.smartregion.com.ua/api/region/v1/';
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {
    const loader = false;
    const files = [];
    const fileStatus = false;
    const allReq = ''
    return {
        loader,
        files,
        fileStatus,
        allReq
    }
}

const getters = {
    getFile(state){
        return state.files
    },
    getFileStatus(state){
        return state.fileStatus
    },
    ALL_REQ(state) {
        return state.allReq
    }
};

const actions = {
    getCards(ctx, data) {
        
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'admin/getAll',
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
            })
            .then((response) => {
                console.log(response.data.appeals)
                ctx.commit('setGetAll', response.data.appeals)
            })
            .catch((error) => {
                console.log(error)
            })
    },
    async getCardDetails(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'admin/details',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
                .then((response) => {
                    
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async createCard(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'admin/create',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
                .then((response) => {
                    
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async getCardDownload(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: 'http://appeal.hotline.kr-admin.gov.ua/api/file/?registration_number=' + data.registration_number +
                '&phone=' + data.phone + '&file_name=' + data.name,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
            })
                .then((response) => {
                    
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

};

const mutations = {
    setFileStatus(state){
        state.fileStatus = false        
    },
    setGetAll(state, arr) {
        state.allReq = arr
    }
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
