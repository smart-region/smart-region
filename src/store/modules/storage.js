function initialState () {
    const base_url = 'https://kr.smartregion.com.ua/api/region/v1/';
    const token = '';
    const snackBar = false;
    const color = '';
    const keyValue = 0;
    const lang = '';
    return {
        base_url,
        token,
        snackBar,
        color,
        keyValue,
        lang
    }
}

const getters = {
    BASE_URL(state) {
        return state.base_url
    },
    getToken(state){
        return state.token
    },
    LANG(state){
        return state.lang
    },
    getBaseUrl(state){
        return state.base_url
    },
    COLOR(state) {
        return state.color
    },
    KEY_VALUE(state) {
        return state.keyValue
    }
};

const mutations = {
    // setBaseUrl() {
    //     base_url
    // },
    setToken(state, token){

        if(token !== 'return'){
            state.token = token
        }
    },
    setSnackBar(state, snackBar){
        state.snackBar = snackBar
    },
    setColor(state, color) {
        state.color = color
    },
    setKeyValue(state, value) {
        if(value) {
            state.keyValue = value
        } else {
            state.keyValue++ 
        }
        
    },
    setBaseUrl(state, url) {
        state.base_url = url
    },
    setLang(state, lang) {
        state.lang = lang
    }
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    mutations
}
