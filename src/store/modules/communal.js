//import { BASE_URL } from "./modulsConsts"

const BASE_URL = 'https://kr.smartregion.com.ua/api/region/v1/';
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {
    const loader = false;
    const companies = [];
    const companyForAddress = [];
    return {
        loader,
        companies,
        companyForAddress
    }
}

const getters = {
    getCompanies(state){
        return state.companies
    },
    GET_COMPANY_FOR_ADDRESS(state) {
        return state.companyForAddress
    }
};

const actions = {
    async getCompanys(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'company',
                timeout: 10000,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },

            })
                .then((response) => {
                    ctx.commit('setCompanies', response.data.company)
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async getCompanyServices(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'companyService/' + data,
                timeout: 10000,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                
            })
                .then((response) => {
                    
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    async getCompanyByAddress(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'accounts/companyLs/' + data,
                timeout: 10000,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                
            })
                .then((response) => {
                    
                    resolve(response)
                    ctx.commit('getCompanyForThisAddress', response.data)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

};

const mutations = {
    setCompanies(state, companies) {
        state.companies = companies
    },
    getCompanyForThisAddress(state, companys) {
        state.companyForAddress = companys
    }
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
