
const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9rci5zbWFydHJlZ2lvbi5jb20udWFcL2FwaVwvcmVnaW9uXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE2MTg5MTAwMjIsImV4cCI6Mzk1MTcxMDAyMiwibmJmIjoxNjE4OTEwMDIyLCJqdGkiOiJrOERBVWdlY0N6VlBySm5NIiwic3ViIjoyMTcsInBydiI6Ijc2ZDU5NWY0MWQwOTYyZDRkMjIzZTFjMTY3NDI2N2E4MmEyMDYzNjYifQ.8U9WWH9OhwQDnlyp-fQmThG4GC2WAaI2Zt9vLrVq9Jc'
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {
    const addres = '';
    const debt = '';
    return {
        addres,
        debt,
    }
}

const getters = {
    SELECTED_ADDRES(state) {
        return state.addres
    },
    DEBT(state) {
        return state.debt
    }
}

const actions = {
    getSelectedAddres({commit}, data) {
        commit('setSelectedAddres', data)
    },

    getDebtForThisCompany(ctx, data) {
        axios(ctx.rootGetters['storage/BASE_URL']+'accounts/debt', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data: data
        })
        .then((response) => {
            let info = response.data.accounts
            let debtInfo = Object.values(info)[0]
            ctx.commit('setDebtForThisAddres', debtInfo)


            // this.debtInfo = debtInfo.service
            // this.ls = debtInfo.ls

        })
        .catch((error) => {
            console.log(error)
        })
    }
}

const mutations = {
    setSelectedAddres(state, value) {
        return state.addres = value
    },
    
    setDebtForThisAddres(state, value) {
        return state.debt = value
    }
}



export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}