const BASE_URL = 'https://kr.smartregion.com.ua/api/region/v1/';
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {
    const debt = [];
    return {
        debt
    }
}

const mutations = {
    GET_DEBT_MUT: (state, debt) => {
        state.debt = debt;
    },
}

const actions = {
    GET_DEBT({commit}, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'accounts/debt', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data
        })
        .then((newFeedback) => {
            commit('ADD_NEW_FEEDBACK', newFeedback.data.feedback); // вызов мутации и в нее прокидываем userInfo
            return newFeedback;
        })
        .catch((error) => {
            console.log(error)
            return error
        })
    },

    GET_FEEDBACK({commit}) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'feedbacks/get', {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((feedbacks) => {
            commit('GET_USER_FEEDBACK', feedbacks.data.feedbacks); // вызов мутации и в нее прокидываем
            return feedbacks;
        })
        .catch((error) => {
            console.log(error)
            return error
        })
    }
}

const getters = {
    FEEDBACK(state) {
        return state.feedbacks
    },
    NEW_FEEDBACK(state) {
        return state.newFeedback
    },
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}