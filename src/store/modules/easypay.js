import axios from "axios";
function initialState () {
        const cardListStatus = ''
        const urlAddCard = ''
        const status = ''
        const dataForPayment = ''
        const redirect_url = ''
        const payInfo = ''
    return {
        cardListStatus,
        urlAddCard,
        status,
        dataForPayment,
        redirect_url,
        payInfo
    }
}

const getters = {
    CARD_STATUS(state) {
        return state.cardListStatus
    },
    URL_ADD_CARD(state) {
        return state.urlAddCard
    },
    PAY_STATUS(state) {
        return state.status
    },
    DATA_FOR_PAY(state) {
        return state.dataForPayment
    },
    REDIRECT_URL(state) {
        return state.redirect_url
    },
    PAY_INFO(state) {
        return state.payInfo
    }

};

const actions = {
    checkCardList(ctx) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'payment/easy-pay/card-list', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            
        })
        .then((response) => {
            // console.log(response.data)
            ctx.commit('setStatusCardList', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    addNewCard(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'payment/easy-pay/3d-secure-url-alt', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data: data
        })
        .then((response) => {
            let url = response.data.url
            console.log(url)
            ctx.commit('setLinkForAddCard', url)
            // console.log(url)
        })
    },

    goPaymentEasyPay(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'payment/easy-pay/payment', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken'],
            },
            data: data
        })
        .then((response) => {
            let redirect_url = response.data.redirect_url
            let payInfo = response.data
            console.log(payInfo)
            
            ctx.commit('setRedirectUrl', redirect_url)
            ctx.commit('setPayInfo', payInfo)
        })
        
    },

    checkStatusPay(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'payment/easy-pay/checkStatus/' + data, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            ctx.commit('setStatus', response.data)
        })
    }
};

const mutations = {
    setStatusCardList(state, value) {
        state.cardListStatus = value
    },
    setLinkForAddCard(state, value) {
        state.urlAddCard = value
    },
    setStatus(state, status) {
        state.status = status
    },
    setDataForPayment(state, dataForPayment) {
        state.dataForPayment = dataForPayment
    },
    setRedirectUrl(state, redirect_url) {
        state.redirect_url = redirect_url
    },
    setPayInfo(state, payInfo) {
        state.payInfo = payInfo
    },
    
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
