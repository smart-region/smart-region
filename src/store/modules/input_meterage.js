
const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9rci5zbWFydHJlZ2lvbi5jb20udWFcL2FwaVwvcmVnaW9uXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE2MTg5MTAwMjIsImV4cCI6Mzk1MTcxMDAyMiwibmJmIjoxNjE4OTEwMDIyLCJqdGkiOiJrOERBVWdlY0N6VlBySm5NIiwic3ViIjoyMTcsInBydiI6Ijc2ZDU5NWY0MWQwOTYyZDRkMjIzZTFjMTY3NDI2N2E4MmEyMDYzNjYifQ.8U9WWH9OhwQDnlyp-fQmThG4GC2WAaI2Zt9vLrVq9Jc'
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'

const state = {
    meterages: {
        meterage: '',
        meterage1: '',
        meterage2: ''
    },
    status: false,
    user_Status: '',
}

const mutations = {
    USER_STATUS(state, userStatus) {
        state.user_Status = userStatus;
    },
    PAYMENT_STATUS(state, status) {
        state.status = status;
    }
}

const actions = {
    inputMeterage(ctx, counter) {
        axios({
            url:ctx.rootGetters['storage/BASE_URL']+'counter/input',
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            data: counter
        })
        .then((response) => {
            const newStatus = response.data.success;
            if(newStatus === true) {
                //commit('PAYMENT_STATUS', newStatus);
            }
            console.log(response)
        })
        .catch((error) => {
            console.log(error)
        })
    }
}

const getters = {
    STATUS(state) {
        return state.status
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}