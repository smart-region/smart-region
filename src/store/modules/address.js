
const BASE_URL = 'https://kr.smartregion.com.ua/api/region/v1/';
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {
    const loader = false;
    const coordinates = '';
    const coordinatesStatus = null;
    const addressesArray = [];
    const loadMap = false;
    const addressPage = 0;
    const disabledBtn = false;
    return {
        loader,
        coordinates,
        coordinatesStatus,
        addressesArray,
        loadMap,
        addressPage,
        disabledBtn
    }
}

const getters = {
    getCoordinates(state){
        // console.log(state.coordinates)
        return state.coordinates
    },
    getCoordinatesStatus(state){
        // console.log(state.coordinatesStatus)
        return state.coordinatesStatus
    },
    getLoadMap(state){
        return state.loadMap
    },
    GET_ALL_ADDRESSES(state){
        return state.addressesArray
    },
    DISABLED_BTN(state) {
        return state.disabledBtn
    }

};

const actions = {
    async getDistrict(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'district',
                timeout: 1000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getCity(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'city',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getStreet(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'street',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getBuilding(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'building',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getCorp(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'corp',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getApartment(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'apartment',


                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async setAddress(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'accounts/add/address',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async setAddressManual(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'newAddress',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async setLicToAddress(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'accounts/addLs',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getGeoAddress(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'geoAddress',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },

    async getListAddresses(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'address', 
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getListApartments(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'apartment',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })


.then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getAccountAddresses(ctx)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'accounts/getAddress',
                timeout: 10000,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
            })
            .then((response) => {
                resolve(response)
                ctx.commit('setAddressArray', response.data.accounts)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
    async getButtonsForAddress(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'category',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data,
            })
            .then((response) => {
                resolve(response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
};

const mutations = {
    setAddressArray(state, addresses){
        state.addressesArray = addresses
    },
    setCoordinatesStatus(state, status){
        state.coordinatesStatus = status
    },
    setCoordinates(state, coordinates) {
        state.coordinates = coordinates
    },
    setDisabledBtn(state, status) {
        state.disabledBtn = status
    }
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}