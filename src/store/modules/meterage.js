import axios from "axios";
function initialState () {
        const countersStatus = ''
        const listCounters = ''
        const addCounterStatus = ''
        const newMeterageStatus = ''
    return {
        countersStatus,
        listCounters,
        addCounterStatus,
        newMeterageStatus
    }
}

const getters = {
    COUNTERS_STATUS(state) {
        return state.countersStatus
    },
    COUNTERS(state) {
        return state.listCounters
    },
    ADD_STATUS(state) {
        return state.addCounterStatus
    },
    NEW_METERAGE_STATUS(state) {
        console.log(state.newMeterageStatus)
        return state.newMeterageStatus
    }
    
 
};

const actions = {
    checkCounters(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'counter/verified/' + data, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            ctx.commit('setCheckCounters', response.data.status)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    getCounters(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'counter/get', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data
        })
        .then((response) => {
            console.log(ctx)
            ctx.commit('setCounters', response.data.counters)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    addCounters(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'counter/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data
        })
        .then((response) => {
            console.log(response)
            ctx.commit('setAddCounterStatus', response.data.success)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    inputMeterage(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'counter/inputTest', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data
        })
        .then((response) => {
            ctx.commit('setStatusNewMeterage', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },


   

    
};

const mutations = {
    setCheckCounters(state, value) {
        state.countersStatus = value
    },

    setCounters(state, value) {
        state.listCounters = value
    },

    setAddCounterStatus(state, status) {
        state.addCounterStatus = status
    },

    addStatusDefault(state) {
        state.addCounterStatus = ''
    },

    setStatusNewMeterage(state, status) {
        state.newMeterageStatus = status
    }
    
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}