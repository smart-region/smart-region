
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {

    const obj = {};
    const arrDebt = [];
    const arrAddres = [];
    const sum = 0;
    const status = false;
    return {
        obj,
        sum,
        arrDebt,
        arrAddres,
        status   
    }
}

const getters = {
    GET_DEBT(state) {
        return state.arrDebt
    },

    GET_ADDRES(state) {
        return state.arrAddres
    },

    GET_SUM(state) {
        return state.sum
    },

    STATUS(state) {
        return state.status
    }
};

const actions = {

    getDebts({ commit }, data) {
        commit('pushArrDebt', data)

    },

    getAddress({ commit }, data) {
        commit('pushArrAddress', data) 
    },

    getOllSum({ commit }, data) {
        commit('pushObj', data) 
    },
    
    clearDebts({ commit }) {
        commit('clearArrDebt')
    }

};

const mutations = {
    pushObj(state, value) {
        state.obj[value.id] = (+value.enteredSum);
        const sumValues = obj => Object.values(obj).reduce((a, b) => a + b);
        state.sum = sumValues(state.obj)
    },

    pushArrDebt(state, value) {
        state.arrDebt.push(value)
    },

    clearArrDebt(state) {
        state.arrDebt.length = 0;
    },

    setStatus(state, value) {
        state.status = value
    }
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
