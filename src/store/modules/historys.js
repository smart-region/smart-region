import axios from "axios";
function initialState () {
        const valHistoryPay = []
        const valHistoryMeterage = []
        
    return {
        valHistoryPay,
        valHistoryMeterage
    }
}

const getters = {
    HISTORY_PAY(state) {
        return state.valHistoryPay
    },
    HISTORY_METERAGE(state) {
        return state.valHistoryMeterage
    },
    
    
    
};

const actions = {
    historyPay(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'payment/history', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('setHistoryPay', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    }, 

    historyMeterage(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'counter/history', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('setHistoryMeterage', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },
};

const mutations = {
    setHistoryPay(state, value) {
        state.valHistoryPay = value
    },   
    setHistoryMeterage(state, value) {
        state.valHistoryMeterage = value
    }, 
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
