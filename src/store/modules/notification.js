import axios from "axios";
function initialState () {
        const listNotifications = ''
        const oneNotification = ''
    return {
        listNotifications,
        oneNotification
    }
}

const getters = {
    LIST_NOTIFICATIONS(state) {
        return state.listNotifications
    },
    ONE_NOTIFICATION(state) {
        return state.oneNotification
    },
    
    
};

const actions = {
    getListNotifications(ctx) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'notifications/getAllNotifications', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            ctx.commit('setListNotifications', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    async deleteNotification(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'notifications/delNotification',
                timeout: 10000,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data

            })
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },

    getOneNotification(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'notifications/getNotification?id=' + data, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            ctx.commit('setOneNotification', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    
};

const mutations = {
    setListNotifications(state, notifications) {
        state.listNotifications = notifications
    },

    setOneNotification(state, notification) {
        state.oneNotification = notification
    },
    
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}