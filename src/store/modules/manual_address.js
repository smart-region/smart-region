import axios from "axios";
function initialState () {
        const cities = []
        const streets = []
        const builds = []
        const corpuses = []
        const apartments = []
        const geoData = ''
        const key = 1
        const keyForMap = 0
        const buildsConfirm = ''
        const confirmed = false
        const coordinates = {
            status: false,
            lat: '',
            lon: ''
        } 
        const infoAddedAddress = {
            city: '',
            street: '',
            building: '',
            corpus: '',
            apartment: '',
            geon_id: '',
            city_id: ''
        }
    
    return {
        cities,
        streets,
        builds,
        corpuses,
        apartments,
        geoData,
        key,
        keyForMap,
        infoAddedAddress,
        buildsConfirm,
        coordinates,
        confirmed
    }
}

const getters = {
    COORDINATES(state) {
        return state.coordinates
    },
    CONFIRMED(state) {
        return state.confirmed
    },
    CITIES(state) {
        return state.cities
    },
    STREETS(state) {
        return state.streets
    },
    BUILDS(state) {
        return state.builds
    },
    BUILDS_CONFIRMS(state) {
        return state.buildsConfirm
    },
    CORPUSES(state) {
        return state.corpuses
    },
    APARTMENTS(state) {
        return state.apartments
    },
    GOE(state) {
        return state.geoData
    },
    KEY_FOR_COMPONENTS(state) {
        return state.key
    },

    KEY_FOR_MAP(state) {
        return state.keyForMap
    },

    SELECTED_CITY(state) {
        return state.infoAddedAddress.city
    },
    SELECTED_STREET(state) {
        return state.infoAddedAddress.street
    },
    SELECTED_BUILDING(state) {
        return state.infoAddedAddress.building
    },
    SELECTED_CORPUS(state) {
        return state.infoAddedAddress.corpus
    },
    SELECTED_APARTMENT(state) {
        return state.infoAddedAddress.apartment
    },
    SELECTED_GEON_ID(state) {
        return state.infoAddedAddress.geon_id
    },
    SELECTED_CITY_ID(state) {
        return state.infoAddedAddress.city_id
    },


};

const actions = {
    getCitiesArr(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'city', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('setCities', response.data.city)
        })
        .catch((error)=> {
            console.log(error)
        })
    }, 

    getStreetsArr(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'street', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('setStreets', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    getBuildsArr(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'build', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('setBuilds', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },
    async getBuildsConfirmArr(ctx, data) {
        return new Promise((resolve, reject) => {
            axios(ctx.rootGetters['storage/BASE_URL']+'confirmedAddress', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data 
            })
            .then((response) => {
                resolve(response)
                console.log(response.data)
                ctx.commit('setBuildsConfirm', response.data)
            })
            .catch((error)=> {
                console.log(error)
            })
        })
    },

    getCorpusArr(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'corpus', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('setCorpuses', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    getApartmentsArr(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'apartment', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data 
        })
        .then((response) => {
            ctx.commit('setApartments', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    async getGeoAddressManual(ctx, data)
    {
        return new Promise((resolve, reject) => {
            axios({
                url: ctx.rootGetters['storage/BASE_URL']+'geoAddress',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
                },
                data: data
            })
            .then((response) => {
                resolve(response)
                console.log(response)
                ctx.commit('setGeoAddress', response)
            })
            .catch((error) => {
                reject(error)
            })
        })
    },
};

const mutations = {
    setCities(state, value) {
        state.cities = value
    },   
    
    setStreets(state, value) {
        state.streets = value
    },

    setBuilds(state, value) {
        state.builds = value
    },

    setBuildsConfirm(state, value) {
        state.buildsConfirm = value
    },

    setCorpuses(state, value) {
        state.corpuses = value
    },

    setApartments(state, value) {
        state.apartments = value
    },

    setGeoAddress(state, value) {
        state.geoData = value
    },

    updateKeyForComponents(state, value) {
        if(value) {
            state.key = value
        } else {
            state.key++
        }
    },

    updateKeyForMap(state, value) {
        state.keyForMap = value
    },

    setInfoAddedAddress(state, data) {
        if(data.all) {
            state.infoAddedAddress.city = data.city
            state.infoAddedAddress.street = data.street
            state.infoAddedAddress.building = data.building
            state.infoAddedAddress.geon_id = data.geon_id
            state.infoAddedAddress.corpus = data.corpus
            state.infoAddedAddress.city_id = data.city_id
            state.infoAddedAddress.apartment = data.apartment
        } else if(data.street) {
            console.log(data.city_id)
            state.infoAddedAddress.street = data.street
            state.infoAddedAddress.city_id = data.city_id
        } else if(data.building) {
            state.infoAddedAddress.building = data.building
            state.infoAddedAddress.geon_id = data.geon_id
            state.infoAddedAddress.corpus = data.corpus
        } else if(data.corpus) {
            state.infoAddedAddress.corpus = data.corpus
        } else if(data.apartment) {
            state.infoAddedAddress.apartment = data.apartment
        } else if(data.city) {
            state.infoAddedAddress.city = data.city
        }
        
    },

    setShowGeo(state, value) {
        state.coordinates.status = true
        state.coordinates.lat = value.latitude
        state.coordinates.lon = value.longitude
    },

    setConfirmed(state, value) {
        state.confirmed = value
    },

    clearAddress(state) {
        state.infoAddedAddress = {
            city: '',
            street: '',
            building: '',
            corpus: '',
            apartment: '',
            geon_id: '',
            city_id: ''
        },
        state.key = 1
        state.coordinates.status = false
    }


};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
