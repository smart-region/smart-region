const BASE_URL = 'https://kr.smartregion.com.ua/api/region/v1/';
import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {
    const feedbacks = [];
    const newFeedback = [];
    const text = '';
    return {
        feedbacks,
        newFeedback,
        text
    }
}

const mutations = {
    GET_USER_FEEDBACK: (state, feedbacks) => {
        state.feedbacks = feedbacks;
    },
    ADD_NEW_FEEDBACK: (state, newFeedback) => {
        state.newFeedback = newFeedback;
    },
}

const actions = {
    ADD_FEEDBACK({commit}, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'feedbacks/add', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data: {
                data,
                address_id: "15",
                company: "1",
            }
        })
        .then((newFeedback) => {
            commit('ADD_NEW_FEEDBACK', newFeedback.data.feedback); // вызов мутации и в нее прокидываем userInfo
            return newFeedback;
        })
        .catch((error) => {
            console.log(error)
            return error
        })
    },

    GET_FEEDBACK(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'feedbacks/get?'+ data, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((feedbacks) => {
            ctx.commit('GET_USER_FEEDBACK', feedbacks.data.feedbacks); // вызов мутации и в нее прокидываем
            return feedbacks;
        })
        .catch((error) => {
            console.log(error)
            return error
        })
    }
}

const getters = {
    FEEDBACK(state) {
        return state.feedbacks
    },
    NEW_FEEDBACK(state) {
        return state.newFeedback
    },
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}