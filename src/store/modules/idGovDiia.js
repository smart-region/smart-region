import axios from "axios";
function initialState () {
        const idState = ''
        const url = ''
        const userInfo = ''
        const verifiedStatus = ''
    return {
        idState,
        url,
        userInfo,
        verifiedStatus
    }
}

const getters = {
    VERIFIED_STATUS(state) {
        return state.verifiedStatus
    },
    STATE(state) {
        return state.idState
    },
    URL(state) {
        return state.url 
    },
    USER_INFO_FROM_DIIA(state) {
        return state.userInfo
    }
    
};

const actions = {
    checkDiiaVerified(ctx) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'dia/verified', {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            
        })
        .then((response) => {
            console.log(response.data.status)
            ctx.commit('setVerifiedStatus', response.data.status)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    createState(ctx) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'state', {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            
        })
        .then((response) => {
            ctx.commit('setState', response.data.state)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    govID(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'dia/bank_id/' + data, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            console.log(response.data.url)
            ctx.commit('setUrl', response.data.url)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    userInfoFromDIIA(ctx) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'dia/me', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            ctx.commit('setUserInfo', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    }

    
};

const mutations = {
    setState(state, value) {
        state.idState = value
    },
    setUrl(state, url) {
        state.url = url
    },
    setUserInfo(state, info) {
        state.userInfo = info
    },
    setVerifiedStatus(state, status) {
        state.verifiedStatus = status
    }
    
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
