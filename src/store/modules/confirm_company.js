import axios from "axios";
import {mapGetters, mapMutations} from 'vuex'
function initialState () {

    const delComp = 0;
    const companyID = 0;
    const companyInfo = ''
    return {
        delComp,
        companyID,
        companyInfo
    }
}

const getters = {
    DEL_COMPANY(state) {
        return state.delComp
    },
    COMPANY_ID(state) {
        return state.companyID
    },
    COMPANY_INFO(state) {
        return state.companyInfo
    },

};

const actions = {

    deleteCompany(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'company/' + data, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
        })
        .then((response) => {
            
            ctx.commit('deleteCompanydStatus', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    },

    confirmCompany(ctx, data) {
        return axios(ctx.rootGetters['storage/BASE_URL']+'serviceVerified', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + ctx.rootGetters['storage/getToken']
            },
            data
        })
        .then((response) => {
            ctx.commit('deleteCompanydStatus', response.data)
        })
        .catch((error)=> {
            console.log(error)
        })
    }, 

    getCompanyID(ctx, data) {
        ctx.commit('setCompanyID', data)
    }
};

const mutations = {
    deleteCompanydStatus(state, value) {
        if (value.success === true) {
            state.delComp++
        }
    },
    setCompanyID(state, id) {
        state.companyID = id
    },
    setCompanyInfo(state, value) {
        state.companyInfo = value
    }
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
