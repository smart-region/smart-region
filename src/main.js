import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import moment from 'moment'
import AxiosPlugin from 'vue-axios-cors';
import vuetify from './plugins/vuetify';
import 'leaflet/dist/leaflet.css';
import './externalScripts/nativescript-webview-interface.js'
import Tawk from 'vue-tawk/src/lib'
import VueRouter from 'vue-router'


Vue.use(VueRouter);
Vue.config.productionTip = false
Vue.use(AxiosPlugin)
Vue.use(Tawk, {
  tawkSrc: 'https://embed.tawk.to/611aac2b649e0a0a5cd16bba/1fd8316vb'
}) 
window.Tawk_API.onLoad = function() {
  window.Tawk_API.hideWidget();
}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')



var oWebViewInterface = window.nsWebViewInterface;
oWebViewInterface.on('setCoordinates', function(coordinates){
    if(Object.keys(coordinates).length == 0){
        store.state.address.coordinatesStatus = false
  	} else {
  		  store.state.address.coordinatesStatus = true	
  	}
  	store.state.address.coordinates = coordinates
    console.log(coordinates)

});
