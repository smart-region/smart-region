module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
   // proxy: 'https://ecg.upc.ua/',
      proxy: {
        '/api': {
          target: 'https://ecg.upc.ua',
          changeOrigin: true,
          pathRewrite: {
            '^/api': ''
          }
        }
    }
}
}